#!/bin/bash

for i in H F Br CH CF CBr CHF CHBr CFBr CBr2 CHFBr CHBr2 CFBr2 CHFBr2
do
	E=$(grep "E(UB3LYP)" $i.log | tail -1 | tr -s " " | cut -d " " -f 6)
    if [ -z $E ]
    then
        E=$(grep "E(RB3LYP)" $i.log | tail -1 | tr -s " " | cut -d " " -f 6)
    fi
	ZPE=$(grep -A 1 "Zero-point vibrational energy" $i.log | tail -1 | tr -s " " | cut -d " " -f 2)

	echo "$E, $ZPE"
done