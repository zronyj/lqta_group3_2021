#!/bin/bash

for i in H F Br CH CF CBr CHF CHBr CFBr CBr2 CHFBr CHBr2 CFBr2 CHFBr2
do
	echo "$i **" >> resultados.csv
	python3 g16_extractor.py -t $i.log | head -2 | tail -1 >> resultados.csv
	echo " --" >> resultados.csv
	python3 g16_extractor.py -t $i.log | head -3 | tail -1 >> resultados.csv
done