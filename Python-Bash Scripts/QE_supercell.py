# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import numpy as np
from matplotlib import pyplot as plt

# -----------------------------------------------------------------------------
# Change this data to your convenience
# -----------------------------------------------------------------------------
name = 'Pd'
aA = 7.4537
xrepeat = 3
yrepeat = 3
layers = 6
lattice = 'FCC'
origin = np.array([[0],[0],[0]])
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

# Bohr to Angstrom
a = aA * 0.529177

# Dictionary with all the vectors to compute a slab
crystal = { 'FCC': {'A':[], 'B':[], 'C':[]}, 'HCP': {'A':[], 'B':[]} }

# Vectors for the first layer of FCC
crystal['FCC']['A'].append( a * np.sqrt(2)/2 * np.array([[1],[0],[0]]) )
crystal['FCC']['A'].append( a * np.sqrt(2)/2 * np.array([[1/2],[np.sqrt(3)/2],[0]]) )
crystal['FCC']['A'].append( a * np.sqrt(3) * np.array([[0],[0],[1]]) )

# Vectors for the second and third layers of FCC
crystal['FCC']['B'] = [1/3 * a1 for a1 in crystal['FCC']['A']]
crystal['FCC']['C'] = [2/3 * a1 for a1 in crystal['FCC']['A']]

# Vectors for the first and second layers of HCP
crystal['HCP']['A'] = [ i for i in crystal['FCC']['A']]
crystal['HCP']['B'] = [ i for i in crystal['FCC']['B']]

# List with all the coordinates of the atoms of the slab
new_slab_data = []

# Get the list with the selected layers for a slab
niveles = list(crystal[lattice].keys())

# Compute the lattice vectors for the unit cell
v1 = crystal[lattice]['A'][0]
v2 = crystal[lattice]['A'][1]
v3 = crystal[lattice]['A'][2]
print('\n\nLattice vectors (unit cell)')
print('-'*64)
print(f'{v1[0,0]:.15f}\t{v1[1,0]:.15f}\t{v1[2,0]:.15f}')
print(f'{v2[0,0]:.15f}\t{v2[1,0]:.15f}\t{v2[2,0]:.15f}')
print(f'{v3[0,0]:.15f}\t{v3[1,0]:.15f}\t{v3[2,0]:.15f}')

# Compute the lattice vectors for the slab
v1 = xrepeat * crystal[lattice]['A'][0]
v2 = yrepeat * crystal[lattice]['A'][1]
v3 = (layers - 1) * 1/3 * crystal[lattice]['A'][2]
print('\n\nLattice vectors (slab)')
print('-'*64)
print(f'{v1[0,0]:.15f}\t{v1[1,0]:.15f}\t{v1[2,0]:.15f}')
print(f'{v2[0,0]:.15f}\t{v2[1,0]:.15f}\t{v2[2,0]:.15f}')
print(f'{v3[0,0]:.15f}\t{v3[1,0]:.15f}\t{v3[2,0]:.15f}\n\n')

# Compute the coordinates for the atoms in the slab
for z in range(layers):
    proto_layer = z%len(crystal[lattice])
    if proto_layer != 0:
        step = z/proto_layer
        layer = niveles[proto_layer]
        new_origin = origin + crystal[lattice][layer][0]
        new_origin = new_origin + crystal[lattice][layer][1]
        new_origin = new_origin + step * crystal[lattice][layer][2]
    else:
        step = z/len(niveles)
        layer = niveles[proto_layer]
        new_origin = origin + step * crystal[lattice][layer][2]
    for y in range(yrepeat):
        move_y = new_origin + y * crystal[lattice]['A'][1]
        for x in range(xrepeat):
            new_atom = move_y + x * crystal[lattice]['A'][0]
            new_slab_data.append( f'{name}\t{new_atom[0][0]:.15f}\t\t{new_atom[1][0]:.15f}\t\t{new_atom[2][0]:.15f}\t\t0\t0\t0\n' )

# Invert the Z coordinates for periodic calculations with a gap
inverted = []
for l in range(len(new_slab_data)):
    lu = new_slab_data[l].split()
    ld = new_slab_data[-1*l-1].split()
    inverted.append(f'{ld[0]}\t{ld[1]}\t\t{ld[2]}\t\t{lu[3]}\t\t{ld[4]}\t{ld[5]}\t{ld[6]}\n')


# -----------------------------------------------------------------------------
# Saving files
# -----------------------------------------------------------------------------

# Save the slab's coordinates as an XYZ file
with open('cell.xyz', 'w') as f:
    f.writelines([str(xrepeat * yrepeat * layers) + '\n', "\n"] + new_slab_data)

# Save the slab's coordinates for calculations with QE
with open('cell.dat', 'w') as f:
    f.writelines(inverted[::-1])