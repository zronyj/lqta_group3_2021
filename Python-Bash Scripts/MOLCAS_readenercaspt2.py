import os
import sys
import matplotlib.pyplot as plt
import math
#read from file where the first column is the converged iteration (output sacamep)
with open(sys.argv[1], "r") as f:
    data = f.readlines()

itera=[]
tabla=[]
#save converged iterations
for i in range(len(data)):
    line=data[i].split()
    itera.append(int(line[0]))

#open all caspt2 outputs for the corresponding iterations and save their energies
for j in itera:
    name=sys.argv[2]+"."+str(j)+".in.out"
    with open(name, "r") as g:
       poncho = g.readlines()
    for i in range(len(poncho)):
        if "Total CASPT2 energies:" in poncho[i]:
            it=[]
            for k in range(3):
                temp=poncho[i+1+k].split()
                it.append(float(temp[6]))
            tabla.append("{0} {1} {2} {3}\n".format(j,it[0],it[1],it[2])) 
#write results in the table to be plotted with plot.py
with open("enercaspt2.dat", "w") as p:
    p.writelines(tabla)
