# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import sys
import matplotlib
matplotlib.rcParams['text.usetex'] = True # To use LaTeX in MatPlotLib :)
from matplotlib import pyplot as plt

# -----------------------------------------------------------------------------
# Working with the data from MOLCAS
# -----------------------------------------------------------------------------

# Get the data
with open(sys.argv[1], "r") as f:
    data = f.readlines()

# Extract the important data
CASSCF_tabla = ["Distancia, Energia\n"]
CASPT2_tabla = ["Distancia, Energia CASSCF", "Energia CASPT2\n"]
X = []
CASSCF_Y = []
CASPT2_Y = []
for i in range(len(data)):
    if "Values of the primitive" in data[i]:
        temp = data[i+2].split()
        distancia = temp[4]
    if "Geometry is converged in" in data[i]:
        temp2 = data[i-13].split()
        CASSCF_tabla.append(f"{distancia}, {temp2[1]}\n")
        X.append(float(distancia))
        CASSCF_Y.append(float(temp2[1]))
    if "Total CASPT2 energies:" in data [i]:
        temp3 = data[i+1].split()
        CASPT2_tabla.append(f"{distancia}, {temp3[6]}\n")
        CASPT2_Y.append(float(temp3[6]))

# -----------------------------------------------------------------------------
# Saving files
# -----------------------------------------------------------------------------

# Save that CASSCF energies and distances
with open("distanciasCASSCF.csv", "w") as g:
    g.writelines(CASSCF_tabla)

# Save that CASPT2 energies and distances
with open("distanciasCASPT2.csv", "w") as h:
    h.writelines(CASPT2_tabla[1:])

# -----------------------------------------------------------------------------
# Working the plot
# -----------------------------------------------------------------------------

# Open the file with the distances of SCF calculation and extract the data
with open("distancias.csv", "r") as ff:
    scf_raw = ff.readlines()

# Turn the data from text to numbers
SCF = [i.split(",") for i in scf_raw]
SCF = [[float(j[0]), float(j[1])] for j in SCF[1:]]
SCF_X, SCF_Y = zip(*SCF)

# Find the minima of all of them and set them to 0 at their minima
tot_scf = len(SCF_X)
tot_CASSCF = len(X)
bajo_scf = min(SCF_Y)
bajo_casscf = min(CASSCF_Y)
bajo_caspt2 = min(CASPT2_Y)
SCF_Y = [2625.5*(y - bajo_scf) for y in SCF_Y]
CASSCF_Y = [2625.5*(y - bajo_casscf) for y in CASSCF_Y]
CASPT2_Y = [2625.5*(y - bajo_caspt2) for y in CASPT2_Y]

# Don't plot beyond this point!
fin = min(tot_scf, tot_CASSCF)

# Plot the energy vs distance for all 3 calculations
plt.plot(SCF_X[:fin], SCF_Y[:fin], 'ko-', label="HF-SCF (aug-cc-pVDZ)")
plt.plot(X[:fin], CASSCF_Y[:fin], 'ro-', label="CASSCF (aug-cc-pVDZ)")
plt.plot(X[:fin], CASPT2_Y[:fin], 'bo-', label="CASPT2 (aug-cc-pVDZ)")
plt.xlabel(r"Distance (C-Br) / $\AA$")
plt.ylabel(r"Energy / $kJ \cdot mol^{-1}$")
plt.legend(loc="best")
plt.savefig("dist_v_energ.png", dpi=300) # Save the file