# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import numpy as np
from scipy.spatial.transform import Rotation as R

# -----------------------------------------------------------------------------
# Change this data to your convenience (data in angstrom!)
# -----------------------------------------------------------------------------
center = "C"
diver = 'Br1'
distance_to_slab = 3.0
molecule = """C       17.126712747   9.895727364  29.102930806
H       16.183720942   9.478439854  29.480649619
F       17.269611162  11.183330290  29.525643288
Br      17.062150226   9.852625066  27.153523818
Br      18.585856754   8.810425078  29.809746555"""
name_of_slab = 'cell.dat'
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Molecule text to object
# -----------------------------------------------------------------------------

# Exctract the data from the molecule to be set on top of the slab
proto_atoms = molecule.split('\n')
frag_atoms = [a.split() for a in proto_atoms]
atoms = {}

# Careful with repeated atoms! Using numbering, but can't handle more than 2
for a in frag_atoms:
    if a[0] in atoms.keys():
        atoms[a[0] + "1"] = atoms[a[0]]
        del atoms[a[0]]
        atoms[a[0] + "2"] = np.array([float(i) for i in a[1:]])
    else:
        atoms[a[0]] = np.array([float(i) for i in a[1:]])

# Remove number from atoms
def denumber(id):
    if id[-1] in '1234567890':
        return id[:-1]
    else:
        return id

# -----------------------------------------------------------------------------
# Working the slab
# -----------------------------------------------------------------------------

# Getting the slab's info
sX = []
sY = []
sZ = []

with open(name_of_slab, 'r') as f:
    data = f.readlines()

# Storing values in lists
for l in data:
    temp = l.split()
    sX.append(float(temp[1]))
    sY.append(float(temp[2]))
    sZ.append(float(temp[3]))

# Getting the two last atoms
a1 = np.array([sX[0], sY[0], sZ[0]])
a2 = np.array([sX[1], sY[1], sZ[1]])
v1 = a2 - a1
e1 = v1 / np.linalg.norm(v1)

# Getting a third non-colinear atom
for b in range(2, len(data)):
    a3 = np.array([sX[b], sY[b], sZ[b]])
    v2 = a3 - a1
    e2 = v2 / np.linalg.norm(v2)
    if np.linalg.norm(e1-e2) > 1e-5:
        break

# Cross product of 2 non-colinear vectors
v3 = np.cross(e1, e2)
e3 = v3 / np.linalg.norm(v3)

# Make sure the vector is facing up
if e3[2] < 0:
    e3 *= -1

# Finding the center of the slab
index = 1
ctrl = 0
while True:
    if sZ[index] != sZ[index - 1]:
        break
    if index == len(data):
        raise Exception('Something is really wrong with this layer.')
    index += 1

# Find the point in the center of the slab
mX = min(sX[:index]) + ( max(sX[:index]) - min(sX[:index]) ) / 2
mY = min(sY[:index]) + ( max(sY[:index]) - min(sY[:index]) ) / 2
mZ = sZ[0]

# -----------------------------------------------------------------------------
# Working the molecule
# -----------------------------------------------------------------------------

# Bringing molecule to origin
atoms_origin = {key: value - atoms[center] for key, value in atoms.items()}

# Normalized vector of the atom of interest
eD = atoms_origin[diver] / np.linalg.norm(atoms_origin[diver])

# Estimating the angle between the atom and the vector facing down
disalignement = np.dot(eD, np.array([0.,0.,-1.]))
angle = np.arccos(disalignement)

# Finding the normal/rotation vector to rotate the molecule
rota_vec = np.cross(eD, np.array([0.,0.,-1.]))

# Computing the rotation matrix
rota_mat = R.from_rotvec(angle * rota_vec)

# Rotating the molecule
atoms_aligned = {key: rota_mat.apply(value) for key, value in atoms_origin.items()}

# -----------------------------------------------------------------------------
# Situating the molecule
# -----------------------------------------------------------------------------

# Creating the displacement vector
mV = mX * np.array([1,0,0]) +\
    mY * np.array([0,1,0]) +\
        (abs(atoms_aligned[diver][2]) + mZ + distance_to_slab) * np.array([0,0,1])

# Moving the molecule
atoms_new = {key: value + mV for key, value in atoms_aligned.items()}

# Preparing coordinates
output = []
for key, value in atoms_new.items():
    output.append(f'{denumber(key)}\t{value[0]:.15f}\t\t{value[1]:.15f}\t\t{value[2]:.15f}\t\t0\t0\t0\n')

# -----------------------------------------------------------------------------
# Saving files
# -----------------------------------------------------------------------------

# Saving molecule as XYZ in new position
with open('molecule.xyz', 'w') as g:
    g.writelines([str(len(atoms_new)) + "\n","\n"] + output)

# Saving the whole system in XYZ
with open('ads.xyz', 'w') as h:
    h.writelines([str(len(atoms_new) + len(data)) + "\n","\n"] + output + data)

# Saving the data for QE calculations
with open('adsor.dat', 'w') as k:
    k.writelines(output + data)