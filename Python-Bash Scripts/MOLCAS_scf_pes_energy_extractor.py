# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import sys
from matplotlib import pyplot as plt

# -----------------------------------------------------------------------------
# Working with the data from MOLCAS
# -----------------------------------------------------------------------------

# Get the data
with open(sys.argv[1], "r") as f:
    data = f.readlines()

# Extract the important data
tabla = ["Distancia, Energia\n"]
X = []
Y = []
for i in range(len(data)):
    if "Values of the primitive" in data[i]:
        temp = data[i+2].split()
        distancia = temp[4]
    if "Geometry is converged in" in data[i]:
        temp2 = data[i-13].split()
        tabla.append("{0}, {1}\n".format(distancia, temp2[1]))
        X.append(float(distancia))
        Y.append(float(temp2[1]))

# -----------------------------------------------------------------------------
# Saving files
# -----------------------------------------------------------------------------

# Save that SCF energies and distances
with open("distancias.csv", "w") as g:
    g.writelines(tabla)

# -----------------------------------------------------------------------------
# Working the plot
# -----------------------------------------------------------------------------
plt.plot(X, Y, 'bo-')
plt.xlabel("Distancia / A")
plt.ylabel("Energia / Hartree")
#plt.show()
plt.savefig("dist_v_energ.png", dpi=300)