import numpy as np
import sys

def thermochemistry(filename):
   fil = open(filename)

   thermo=[]

   #go to the beginning of the file
   fil.seek(0)
   for line in fil:

   #read until sum...
      if "Sum of electronic and zero-point Energies=" in line:
         l=line

   #storear values in thermo 
         for i in range(3):
            t = 2625.5*float(l.split()[6])
            thermo.append(t)
            l=fil.readline()
         t = 2625.5*float(l.split()[7])
         thermo.append(t)

   fil.close()
   return thermo

#promt user 
react=int(input("Number of reactants: "))

thermor=[]
thermop=[]

#extract thermo for reactants
for i in range(react):
   fil=input("Reactant file: ")
   thermo = thermochemistry(fil)
   thermor.append(thermo)

#promt user
prod=int(input("Number of products: "))

#extract thermo for products
for i in range(prod):
   fil=input("Product file: ")
   thermo = thermochemistry(fil)
   thermop.append(thermo)

#calculate thermo reaction results productos-reactivos
results=[]
for i in range(4):
   result=0
   for j in range(prod):
      result=result+thermop[j][i]
   for j in range(react):
      result=result-thermor[j][i]
   results.append(result)

#print(results)
print(f'Δ(Ε+ZPE)= {results[0]}')
print(f'Δ(Ε+thermalE)= {results[1]}')
print(f'ΔH= {results[2]}')
print(f'ΔG= {results[3]}')


