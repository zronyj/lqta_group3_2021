# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import os
import re
import subprocess
from shutil import copyfile
from matplotlib import pyplot as plt

# Need to know where I am
path = os.getcwd()

# -----------------------------------------------------------------------------
# Change this data to your convenience
# -----------------------------------------------------------------------------
kp = [3, 4, 6, 9, 12, 15, 18, 24, 32]             # K_POINTS
dgauss = [i/1000 for i in range(5, 47, 2)]        # degauss
celldm = [i/10 for i in range(50, 110, 5)]        # celldm
ecutwfc = list(range(5, 55, 5))                   # ecutwfc
# -----------------------------------------------------------------------------
kp0 = 4                                           # fix K_POINTS value
dgauss0 = 3                                       # fix degauss value
celldm0 = 5                                       # fix celldm value
ecutwfc0 = 6                                      # fix ecutwfc value
# -----------------------------------------------------------------------------
name = 'Pd'                                       # Name of the element
template = 'template.in'                          # Name of the template file
pseudop = 'pd_pbe_v1.4.uspp.F.UPF'                # Name of the pseudopotential
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# Creating the object with the fix parameters
pv = {"celldm":celldm[celldm0], "ecutwfc":ecutwfc[ecutwfc0], "kp":kp[kp0],\
	"dgauss":dgauss[dgauss0]}

# Text as input of the running script (bash)
runner = f"""#!/bin/bash
module load espresso/6.3
ulimit -s unlimited
mpirun -n 8 pw.x -i {name}.in > {name}.out"""

# Read the template file
with open(template, 'r') as f:
	entrada = f.read()

# -----------------------------------------------------------------------------
# Function: optimizador_parametro
# -----------------------------------------------------------------------------
# Description: Creates an input file for Quantum ESPRESSO based on the fix
#              values for the given parameters, but changing one of them.
#              It also creates a bash script as a file to run QE with the
#              specified input file. Finally, it runs said script.
# Inputs:
#    - name: The name of the element or job as a string
#    - flag: The name of the parameter to be changed as a string
#    - entrada: The template of the input file as a string
#    - options: A list with the values of the parameter to be changed
#    - path: This script's current path as a string
#    - script: The template of the bash script as a string
#    - pseudop: The name of the pseudopotential file as a string
#    - pv: The object with all the fix values for the parameters as a dict
# Outputs:
#    - The output of running QE with the generated input file.
# -----------------------------------------------------------------------------
def optimizador_parametro(name, flag, entrada, options, path, script, pseudop, pv):
	# For every possible value of a given (flag) parameter ...
	for x in options:
		pv[flag] = x # Change the object with the new value
		xx_input = entrada.format(tcd=pv["celldm"], ecw=pv["ecutwfc"],\
			tkp=pv["kp"], tdg=pv["dgauss"]) # Format the template with the new pv
		wd = f'{name}-{flag}-{x}'.replace(".","") # Name for a new directory
		cwd = f'{path}/{wd}' # Path for the new directory
		os.mkdir(cwd, 0o777) # Create the directory
		copyfile(f'{path}/{pseudop}', f'{cwd}/{pseudop}') # Copy the pseudop file
		os.chdir(cwd) # Get into that directory
		# Write the input file
		with open(f'{name}.in', 'w') as f:
			f.write(xx_input)
		# Write the bash script
		with open('runner.sh', 'w') as g:
			g.write(script)
		# Run the bash script
		code = subprocess.call(['bash', 'runner.sh'])
		# Check if the bash script ran successfully
		if code != 0:
			raise Exception('Quantum ESPRESSO did not finish correctly!')
		# Move back to the main directory
		os.chdir(path)

# -----------------------------------------------------------------------------
# Function: plotteador
# -----------------------------------------------------------------------------
# Description: Creates a plot of the Fermi energy in Quantum ESPRESSO's output
#              file, and the parameter being optimized.
# Inputs:
#    - name: The name of the element or job as a string
#    - flag: The name of the parameter to be changed as a string
#    - path: This script's current path as a string
# Outputs:
#    - A png file with the plot of the Energy vs the given (flag) parameter.
# -----------------------------------------------------------------------------
def plotteador(name, flag, path):
	listos = os.listdir(path) # Check for the files in the current path
	xx_data = {flag:[], 'energy':[]} # Create a dictionary for the X and Y coords
	xx_date = [] # List for the coordinates
	prefix = f'{name}-{flag}' # Prefix of the directories for the given param. 
	for a in listos: # For every file and directory in the current path ...
		# ... check if the prefix is in the name of the item.
		# This would mean that the listed item is a directory.
		if prefix in a:
			cwd = f'{path}/{a}/{name}.out' # Construct the path of the output
			# Read the output file
			with open(cwd, 'r') as f:
				output = f.read()
			# Using regular expressions, look for the line starting with ! and
			# ending with \n, with any number of any character in between
			line = re.search(r'!.*\n', output)
			# Split the string of that line using spaces, \n or tab
			pieces = line.group(0).split()
			# Artifact to extract the decimal number from the directory name
			if (flag == "dgauss") or (flag == "celldm"):
				vaxis = float(f'0.{a[len(prefix)+1:]}') * 10
			else:
				vaxis = float(a[len(prefix)+1:])
			# Create list with the X and Y coordinates and add it to the list
			xx_date.append( [float(pieces[4]), vaxis] )
	# Sort the list with the coordinates
	xx_date.sort(key=lambda x: x[1])
	# Transform the list of lists into two lists of values
	xx_data[flag], xx_data["energy"] = list(zip(*xx_date))
	# Plot the energy vs the parameter being optimized
	plt.plot(xx_data["energy"], xx_data[flag], 'o-')
	# Make it pretty and readable
	plt.title(f'Energy vs {flag}')
	plt.xlabel(f'{flag}')
	plt.ylabel("Energy / Ry")
	# Save the plot as an image at high resolution
	plt.savefig(f'{name}-{flag}.png', dpi=300)
	# Clear the plot for future plots
	plt.figure().clear()

# Optimize ecutwfc
optimizador_parametro(name=name, flag="ecutwfc", entrada=entrada,\
options=ecutwfc, path=path, script=runner, pseudop=pseudop, pv=dict(pv))

# Optimize degauss
optimizador_parametro(name=name, flag="dgauss", entrada=entrada,\
options=dgauss, path=path, script=runner, pseudop=pseudop, pv=dict(pv))

# Optimize celldm
optimizador_parametro(name=name, flag="celldm", entrada=entrada,\
options=celldm, path=path, script=runner, pseudop=pseudop, pv=dict(pv))

# Optimize K_POINTS
optimizador_parametro(name=name, flag="kp", entrada=entrada,\
options=kp, path=path, script=runner, pseudop=pseudop, pv=dict(pv))

# Plot energy vs each parameter optimized
plotteador(name=name, flag="ecutwfc", path=path)
plotteador(name=name, flag="dgauss", path=path)
plotteador(name=name, flag="celldm", path=path)
plotteador(name=name, flag="kp", path=path)