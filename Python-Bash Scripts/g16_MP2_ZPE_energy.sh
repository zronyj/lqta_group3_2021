#!/bin/bash

for i in H F Br CH CF CBr CHF CHBr CFBr CBr2 CHFBr CHBr2 CFBr2 CHFBr2
do
	protoE=$(grep "EUMP2" $i.log | tail -1 | tr -s " " | cut -d " " -f 7)
	E=$(python3 -c """import sys
dato = sys.argv[-1]
datos = dato.split('D+')
print(float(datos[0]) * 10**float(datos[1]))""" $protoE)
	ZPE=$(grep -A 1 "Zero-point vibrational energy" $i.log | tail -1 | tr -s " " | cut -d " " -f 2)

	echo "$E, $ZPE"
done