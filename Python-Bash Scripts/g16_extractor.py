# -----------------------------------------------------------------------------
# Importing libraries
# -----------------------------------------------------------------------------
import sys
import numpy as np
from matplotlib import pyplot as plt

# Getting command line arguments
params = [i[1:] for i in sys.argv if i[0] == "-"]

# Establishing a periodic table
elements = [0, 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F',
'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca',
'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga',
'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo',
'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I',
'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re',
'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk',
'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs',
'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og']

# -----------------------------------------------------------------------------
# Function: get_atoms
# -----------------------------------------------------------------------------
# Description: gets the number of atoms in the calculation
# Inputs:
#    - lines: The information from the Gaussian output as a list of strings
#    - verbose: A boolean flag in case the user wants to see what's being done
# Outputs:
#    - atomos[0]: the number of atoms in the molecule as an integer
# -----------------------------------------------------------------------------
def get_atoms(lines, verbose=False):
    ctrl = []
    for l in lines:
        if "NAtoms=" in l:
            temp = l.split()
            posi = temp.index("NAtoms=")
            ctrl.append( int(temp[posi + 1]) )
    atomos = list(set(ctrl))
    if verbose:
        for a in atomos:
            print('Number of atoms: {}'.format(a))
        print('Working with the first number of atoms.')
    return atomos[0]

# -----------------------------------------------------------------------------
# Function: get_geom
# -----------------------------------------------------------------------------
# Description: extracts the geometry of the calculated molecule
# Inputs:
#    - lines: The information from the Gaussian output as a list of strings
#    - atoms: The number of atoms in the molecule
# Outputs:
#    - Prints the geometry of the molecule as a set of coordinates on screen
# -----------------------------------------------------------------------------
def get_geom(lines, atoms):
    total = len(lines)
    for l in range(total):
        if "Standard orientation" in lines[l]:
            molecula = []
            extension = l + 5
            for a in range(atoms):
                temp = lines[l + 5 + a].split()
                molecula.append( [int(temp[1])] + temp[3:] )
        if "Stationary point found" in lines[l]:
            break
    show = "{0}\nFile produced by Rony's G16 Extractor from a Gaussian output.".format(atoms)
    for a in molecula:
        show += "\n{0}\t{1}\t{2}\t{3}".format(elements[a[0]], *a[1:])
    print(show)

# -----------------------------------------------------------------------------
# Function: get_termochem
# -----------------------------------------------------------------------------
# Description: gets the thermochemistry calculated by Gaussian for a molecule
# Inputs:
#    - lines: The information from the Gaussian output as a list of strings
#    - verbose: A flag in case the user wants to see what's being done
# Outputs:
#    - Prints the thermodynamical energies of the Gaussian output on screen
# -----------------------------------------------------------------------------
def get_termochem(lines, verbose=False):
    ctrl = False
    for l in lines:
        if "Thermochemistry" in l:
            ctrl = True
            if not verbose:
                print("E[0]\tE\tH\tG")
        if "Sum of electronic and zero-point Energies" in l:
            temp = l.split()
            if verbose:
                print("E[0] = E[elec] + ZPE = {0}".format(temp[-1]))
            else:
                print(temp[-1])
        if "Sum of electronic and thermal Energies" in l:
            temp = l.split()
            if verbose:
                print("E = E[0] + E[vib] + E[rot] + E[trans] = {0}".format(temp[-1]))
            else:
                print(temp[-1])
        if "Sum of electronic and thermal Enthalpies" in l:
            temp = l.split()
            if verbose:
                print("H = E + RT = {0}".format(temp[-1]))
            else:
                print(temp[-1])
        if "Sum of electronic and thermal Free Energies" in l:
            temp = l.split()
            if verbose:
                print("G = H - TS = {0}".format(temp[-1]))
            else:
                print(temp[-1])
    if not ctrl:
        print("*\tNo thermochemistry was found in the file.")

# -----------------------------------------------------------------------------
# Function: get_frequencies
# -----------------------------------------------------------------------------
# Description: plots the IR spectrum of the molecule
# Inputs:
#    - lines: The information from the Gaussian output as a list of strings
#    - verbose: A flag in case the user wants to see what's being done
# Outputs:
#    - Creates a PNG file with the IR spectrum of the molecule
# -----------------------------------------------------------------------------
def get_frequencies(lines, verbose=False):
    for l in lines:
        pass

# -----------------------------------------------------------------------------
# Main program
# -----------------------------------------------------------------------------
if ".log" in sys.argv[-1]:
    with open(sys.argv[-1], "r") as f:
        data = f.readlines()
    atoms = get_atoms(data)
    if "g" in params:
        get_geom(data, atoms)
    elif "t" in params:
        get_termochem(data)
    elif "f" in params:
        pass
    else:
        atoms = get_atoms(data, True)
        print('*'*80)
        print('*' + " "*30 + 'OPTIMIZED GEOMETRY' + " "*30 + '*')
        print('*'*80)
        print('\n')
        get_geom(data, atoms)
        print('\n')
        print('*'*80)
        print("*" + " "*32 + "THERMOCHEMISTRY" + " "*31 + "*")
        print('*'*80)
        print('\n')
        get_termochem(data, True)
        print('\n')
        print('*'*80)