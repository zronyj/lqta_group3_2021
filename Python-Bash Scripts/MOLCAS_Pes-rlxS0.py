import os
import sys
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['text.usetex'] = True

with open(sys.argv[1], "r") as f:
    data = f.readlines()

xdist=[]
#CASSCF root 1
yener=[]
#CASPT2 root 1
root1=[]
#CASPT2 root 2
root2=[]
#CASPT2 root 3
root3=[]
#CASSCF root 2
cas2=[]
#CASSCF root 3
cas3=[]
tabla =[] 
for i in range(len(data)):
		#Get distances
    if "Values of the primitive" in data[i]:
        temp = data[i+2].split()
        distancia = temp[4]
		#Get CASSCF energies ground state
    if "Geometry is converged in" in data[i]:
        temp2 = data[i-13].split()
        xdist.append(float(distancia))
        yener.append(float(temp2[1]))
		#Get CASSCF energies excited states
    if ("RASSCF root number  1 Total energy:") in data[i]:
        temp6=data[i+1].split()
        temp7=data[i+2].split()    
		#Get CASPT2 energies
    if "Total CASPT2 energies:" in data [i]:
        temp3 = data[i+1].split()
        temp4 = data[i+2].split()
        temp5 = data[i+3].split()
        tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(distancia,temp2[1],temp6[7],temp7[7], temp3[6],temp4[6],temp5[6]))
        root1.append(float(temp3[6]))
        root2.append(float(temp4[6]))
        root3.append(float(temp5[6]))
        cas2.append(float(temp6[7]))
        cas3.append(float(temp7[7]))

with open("energyresults.csv", "w") as g:
    g.writelines(tabla)
#energies refrred relative to their minimum
mini=min(yener)
yener=[2625.5*(y - mini) for y in yener]
cas2=[2625.5*(y - mini) for y in cas2]
cas3=[2625.5*(y - mini) for y in cas3]
minroot1=min(root1)
root1=[2625.5*(y - minroot1) for y in root1]
root2=[2625.5*(y - minroot1) for y in root2]
root3=[2625.5*(y - minroot1) for y in root3]
#plot energies vs distances
plt.plot(xdist,yener,"rx-",label=r"S$_{0}$ CASSCF (aug-cc-pVDZ)")
plt.plot(xdist,cas2,"bx-",label=r"S$_{1}$ CASSCF (aug-cc-pVDZ)")
plt.plot(xdist,cas3,"kx-",label=r"S$_{2}$ CASSCF (aug-cc-pVDZ)")
plt.plot(xdist,root1,"go-",label=r"S$_{0}$ CASPT2 (aug-cc-pVDZ)")
plt.plot(xdist,root2,"co-",label=r"S$_{1}$ CASPT2 (aug-cc-pVDZ)")
plt.plot(xdist,root3,"mo-",label=r"S$_{2}$ CASPT2 (aug-cc-pVDZ)")
plt.xlabel(r"Distance (C-Br) / $\AA$")
plt.ylabel(r"Energy / $kJ \cdot mol^{-1}$")
plt.legend(loc="best")
plt.show()
