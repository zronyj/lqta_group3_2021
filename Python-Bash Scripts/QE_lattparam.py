import os
import sys
import math

latt_text=input("Enter lattice constant: ")
latt=float(latt_text)
ca1=((math.sqrt(2))/2)*latt
a1=[ca1,0.0,0.0]
a2=[ca1/2,ca1*(math.sqrt(3)/2),0.0]
a3=[0.0,0.0,math.sqrt(3)*latt]

Nt=input("Insert N dimension: ")
N=int(Nt)

atom=input("Enter atomic symbol: ")

tabla = ["ATOMIC POSITIONS (bohr)\n"]
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(atom, 0.0, 0.0, a3[2]*(5/3), 0, 0, 1))
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(atom, a2[0], (1/3)*a2[1], a3[2]*(4/3), 0, 0, 1))
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(atom, a1[0], (2/3)*a2[1], a3[2], 0, 0, 0))
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(atom, 0.0, 0.0, a3[2]*(2/3), 0, 0, 0))
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n".format(atom, a2[0], (1/3)*a2[1], a3[2]*(1/3), 0, 0, 1))
tabla.append("{0} {1} {2} {3} {4} {5} {6}\n\n".format(atom, a1[0], (2/3)*a2[1], 0.0, 0, 0, 1))
tabla.append("CELL_PARAMETERS (bohr)\n")
tabla.append("{0} {1} {2} \n".format(N*a1[0], N*a1[1], N*a1[2]))
tabla.append("{0} {1} {2} \n".format(N*a2[0], N*a2[1], N*a2[2]))
tabla.append("{0} {1} {2} \n".format(a3[0], a3[1], 75))
with open("Coordenadas.dat", "w") as g:
    g.writelines(tabla)
