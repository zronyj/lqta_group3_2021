# Import library
import os

# Check where am I
wd = os.getcwd()
# Get all the items in my current path
lista = os.listdir(wd)

# Template of the bash script to be created
content = """#!/bin/bash

# Load MOPAC module
module load mopac/2016

# Execution line
MOPAC2016.exe {0}/{1}"""

# For every .mop input file, create a bash script to run mopac with that input
for a in lista:
	if ".mop" in a:
		with open(a[:-3]+"sh", "w") as f:
			f.write(content.format(wd, a))