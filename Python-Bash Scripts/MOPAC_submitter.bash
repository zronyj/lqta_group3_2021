#!/bin/bash

# Submit all MOPAC jobs with .sh extension in this folder
# Use 4 processors if possible
for a in `ls *.sh`
do
	chmod +x $a
	sbatch -A emtccm_serv -p emtccm -n 4 ./$a
done