import os
import sys
import matplotlib.pyplot as plt
import math

k=["03","04","06","09","12","15","18","24"]

    
name="results03.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss03=[]
Energy03=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss03.append(float(temp[0]))
    Energy03.append(float(temp[1]))

name="results04.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss04=[]
Energy04=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss04.append(float(temp[0]))
    Energy04.append(float(temp[1]))

name="results06.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss06=[]
Energy06=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss06.append(float(temp[0]))
    Energy06.append(float(temp[1]))

name="results09.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss09=[]
Energy09=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss09.append(float(temp[0]))
    Energy09.append(float(temp[1]))

name="results12.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss12=[]
Energy12=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss12.append(float(temp[0]))
    Energy12.append(float(temp[1]))

name="results15.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss15=[]
Energy15=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss15.append(float(temp[0]))
    Energy15.append(float(temp[1]))

name="results18.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss18=[]
Energy18=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss18.append(float(temp[0]))
    Energy18.append(float(temp[1]))

name="results24.dat"

with open(name, "r") as f:
    data = f.readlines()
Degauss24=[]
Energy24=[]
for i in range(len(data)):
    temp=data[i].split()
    Degauss24.append(float(temp[0]))
    Energy24.append(float(temp[1]))

plt.plot(Degauss03,Energy03,"go-",label="K-Points: 3x3x3")
plt.plot(Degauss04,Energy04,"ro-",label="K-Points: 4x4x4")
plt.plot(Degauss06,Energy06,"co-",label="K-Points: 6x6x6")
plt.plot(Degauss09,Energy09,"bo-",label="K-Points: 9x9x9")
plt.plot(Degauss12,Energy12,"rx-",label="K-Points: 12x12x12")
plt.plot(Degauss15,Energy15,"gx-",label="K-Points: 15x15x15")
plt.plot(Degauss18,Energy18,"cx-",label="K-Points: 18x18x18")
plt.plot(Degauss24,Energy24,"bx-",label="K-Points: 24x24x24")
plt.ylabel("Energy (Ry)")
plt.xlabel("Degauss (Ry)")
plt.legend(loc="best")
plt.show()
