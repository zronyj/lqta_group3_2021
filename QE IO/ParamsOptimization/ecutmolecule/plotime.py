import numpy as np
import sys
import os
import matplotlib.pyplot as plt

ecut=[5,10,15,20,25,30,35,40,45,50]
CPU=[10.26,26.43,44.57,60+17.56,60+50.36,120+33.78,120+58.10,180+38.61,240+37.83,300+14.68]
wall=[11.83,29.84,49.66,60+25.40,120+1.08,120+47.43,180+16.17,180+59.78,300+3.06,300+44.56]
res=[]

for i in range(10):
	res.append(CPU[i]+wall[i])

plt.plot(ecut,res,"ro-")
plt.ylabel("Time (s)")
plt.xlabel("ecutwfc (Ry)")
plt.show()


