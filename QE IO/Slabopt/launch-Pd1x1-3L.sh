#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --job-name=Al-1x1-6L
#SBATCH --account=emtccm_serv
#SBATCH --partition=emtccm


module load espresso/6.3

ulimit -s unlimited

echo ---------------------------------
echo 
echo "Job running at ${SLURM_JOB_NUM_NODES} node(s): ${SLURM_JOB_NODELIST}"
echo 
date
echo 
echo ---------------------------------


mpirun -n $SLURM_NTASKS pw.x -i pw_Pd1x1-3L.in > pw_Pd1x1-3L.out

echo 
echo Done!
date
echo 
echo ---------------------------------


