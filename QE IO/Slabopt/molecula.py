import os
import sys
import math
C=[-0.099584431, 0.784174519, 0.000000000]
H=[-1.015693409, 1.389075826, 0.000000000]
F=[ 0.994172218, 1.596411826, 0.000000000]
Br1=[-0.104594189, -0.302140085, 1.619643427]
Br2=[-0.104594189, -0.302140085, -1.619643427]

#Calculate distances
dCH = math.sqrt(((C[0]-H[0])**2)+((C[1]-H[1])**2)+((C[2]-H[2])**2))
dCF = math.sqrt(((C[0]-F[0])**2)+((C[1]-F[1])**2)+((C[2]-F[2])**2))
dCBr1 = math.sqrt(((C[0]-Br1[0])**2)+((C[1]-Br1[1])**2)+((C[2]-Br1[2])**2))
dCBr2 = math.sqrt(((C[0]-Br2[0])**2)+((C[1]-Br2[1])**2)+((C[2]-Br2[2])**2))
dHF = math.sqrt(((F[0]-H[0])**2)+((F[1]-H[1])**2)+((F[2]-H[2])**2))
dBr1F = math.sqrt(((F[0]-Br1[0])**2)+((F[1]-Br1[1])**2)+((F[2]-Br1[2])**2))
dHBr1 = math.sqrt(((Br1[0]-H[0])**2)+((Br1[1]-H[1])**2)+((Br1[2]-H[2])**2))
dBrBr = math.sqrt(((Br1[0]-Br2[0])**2)+((Br1[1]-Br2[1])**2)+((Br1[2]-Br2[2])**2))

#Calculate angles
aHCF = math.acos((dCF**2+dCH**2-dHF**2)/(2*dCF*dCH)) 
aFCBr1 = math.acos((dCF**2+dCBr1**2-dBr1F**2)/(2*dCF*dCBr1)) 
aHCBr1 = math.acos((dCBr1**2+dCH**2-dHBr1**2)/(2*dCBr1*dCH)) 
#Calculate F position
a=aHCF-(math.pi/2)
yF=dCF*math.cos(a)
zF=dCF*math.sin(a)

#Calculate Br position
b=aHCBr1-(math.pi/2)
c=aFCBr1-(math.pi/2)
zBr=dCBr1*math.sin(b)
yBr=dCBr1*math.sin(c)
xBr=dBrBr/2

#Final Coordinates
Hn=[1.3945300440,    0.8051322970,  7.5512799180]
Cn=[Hn[0],Hn[1],Hn[2]+dCH]
Fn=[Hn[0],Hn[1]+yF,Cn[2]+zF]
Br1n=[Hn[0]+xBr,Cn[1]-yBr,Cn[2]+zBr]
Br2n=[Hn[0]-xBr,Cn[1]-yBr,Cn[2]+zBr]
print(Cn)
print(Fn)
print(Br1n)
print(Br2n)
print(dCF)
dCFn = math.sqrt(((Cn[0]-Fn[0])**2)+((Cn[1]-Fn[1])**2)+((Cn[2]-Fn[2])**2))
print(dCFn)
