#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=32
#SBATCH --job-name=CHFBr333
#SBATCH --account=moleculas_serv
#SBATCH --partition=bigp


module load espresso/6.3

ulimit -s unlimited

echo ---------------------------------
echo 
echo "Job running at ${SLURM_JOB_NUM_NODES} node(s): ${SLURM_JOB_NODELIST}"
echo 
date
echo 
echo ---------------------------------


mpirun -n $SLURM_NTASKS pw.x -i adsorption333.in > adsorption333.out

echo 
echo Done!
date
echo 
echo ---------------------------------


