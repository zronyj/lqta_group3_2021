#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --job-name=Al-kp-conv
#SBATCH --account=emtccm_serv
#SBATCH --partition=emtccm

module load espresso/6.3

ulimit -s unlimited

echo ---------------------------------
echo 
echo "Job running at ${SLURM_JOB_NUM_NODES} node(s): ${SLURM_JOB_NODELIST}"
echo 
date
echo 
echo ---------------------------------

b=6.5

for kp in 3 4 6 9 12 15 18 24
do

for i in {1..21}
do 

## Here adjust to obtain the wanted variation of the variable
 a=$(echo "scale=2; ($b + (($i-1) * (0.1)))" | bc)

echo !!
echo "Current step"
echo "Calculation for the value:"
echo $kp
echo $a
echo !!


mkdir kp-$kp
cd kp-$kp
cp ../pw_in_seed.in pw-input.in
cp ../pd_pbe_v1.4.uspp.F.UPF .

sed -i -e "s/labkp/$kp/g" pw-input.in
sed -i -e "s/labacell/$a/g" pw-input.in


mpirun -n $SLURM_NTASKS pw.x -i pw-input.in > pw-output.out

## Here make sure to rename all the needed files (DEPENDS ON THE RUN!)
mv pw-input.in pw-$a.in
mv pw-output.out pw-$a.out
mv prefix.save pw-$a.save
mv prefix.xml pw-$a.xml

echo !!
echo $kp
echo "End of step"
echo $i
echo !!
echo
echo

cd ..

done

done

echo 
echo Done!
date
echo 
echo ---------------------------------


