import numpy as np
import sys
import os
import matplotlib.pyplot as plt

ecut=[]
ener=[]
tabla=[]
b=25
name1 = "pw-"
name2 = ".out"
#nf=int(os.system("ls pw-*.out | wc -l"))
#print(nf)
for i in range(6):
  temp=b+i*5
  ecut.append(float(temp))
  filename=name1+str(temp)+name2
  ecutfile=open(filename)
  ecutfile.seek(0)
  for line in ecutfile:
    if "!" in line:
      temp2=line.split()
      ener.append(float(temp2[4]))
  tabla.append("{0} {1} {2}\n".format(i,ecut[i],ener[i]))
#print(ener[6]-ener[7])

plt.plot(ecut,ener,"ro-")
plt.ylabel("Energy (Ry)")
plt.xlabel("ecutwfc (Ry)")
plt.show()

with open("ecut.dat", "w") as p:
    p.writelines(tabla)

