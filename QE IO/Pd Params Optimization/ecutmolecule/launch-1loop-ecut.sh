#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --job-name=H2-ecut-conv
#SBATCH --account=emtccm_serv
#SBATCH --partition=emtccm
##SBATCH --time=7-00:00:00

module load espresso/6.3

ulimit -s unlimited

echo ---------------------------------
echo 
echo "Job running at ${SLURM_JOB_NUM_NODES} node(s): ${SLURM_JOB_NODELIST}"
echo 
date
echo 
echo ---------------------------------

## Input here the starting value of the quantity
b=5

## Adjust here the range of i to obtain the desired variatio ( at the i-th step the value is obtained using (i-1))
for i in {1..10}
do 

## Here adjust to obtain the wanted variation of the variable
 a=$(echo "scale=2; ($b + (($i-1) * (5)))" | bc)

echo !!
echo "Current step"
echo $i
echo "Calculation for the value:"
echo $a
echo !!

## search and substitute all the "lablab" found in the pw_in_seed.in file with the value of a.

sed -e "s/lablab/$a/g" pw_in_seed.in > pw-input.in

mpirun -n $SLURM_NTASKS pw.x -i pw-input.in > pw-output.out

## Here make sure to rename all the needed files (DEPENDS ON THE RUN!)
mv pw-input.in pw-$a.in
mv pw-output.out pw-$a.out
mv prefix.save pw-$a.save
mv prefix.xml pw-$a.xml

echo !!
echo "End of step"
echo $i
echo !!
echo
echo




done

echo 
echo Done!
date
echo 
echo ---------------------------------


