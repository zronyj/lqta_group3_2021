#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --job-name=H2-relax
#SBATCH --account=emtccm_serv
#SBATCH --partition=emtccm
##SBATCH --time=7-00:00:00

module load espresso/6.3

ulimit -s unlimited

echo ---------------------------------
echo 
echo "Job running at ${SLURM_JOB_NUM_NODES} node(s): ${SLURM_JOB_NODELIST}"
echo 
date
echo 
echo ---------------------------------


mpirun -n $SLURM_NTASKS pw.x -i pw.in > pw.out

echo 
echo Done!
date
echo 
echo ---------------------------------


