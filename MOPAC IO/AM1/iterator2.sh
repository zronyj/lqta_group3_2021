#!/bin/bash

for i in H F Br CH CF CBr CHF CHBr CFBr CBr2 CHFBr CHBr2 CFBr2 CHFBr2
do
	RONY1=$(python3 mopac_extractor.py $i.out | wc -l)
	echo "$i **" >> resultados2.csv
	python3 mopac_extractor.py $i.out | head -2 | tail -1 >> resultados2.csv
	echo " --" >> resultados2.csv
	if [ $RONY1 -gt 2 ]; then
		python3 mopac_extractor.py $i.out | tail -1 >> resultados2.csv
	fi
done