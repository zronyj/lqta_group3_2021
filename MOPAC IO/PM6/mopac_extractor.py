import sys

with open(sys.argv[-1], 'r') as f:
    for l in f.readlines():
        if "TOTAL ENERGY" in l:
            temp1 = l.split()
            print( "E =\n{}".format(float(temp1[3])) )
        if "ZERO POINT ENERGY" in l:
            temp2 = l.split()
            print( "ZPE =\n{}".format(float(temp2[3])) )