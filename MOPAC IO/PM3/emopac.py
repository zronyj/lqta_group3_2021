import numpy as np
import sys

#filename=sys.argv[1]
def mopace(filename):
   fil = open(filename)

   ener=[]

   #ir al principio del file
   fil.seek(0)
   for line in fil:

   #leer hasta que salga sum...
      if "TOTAL ENERGY" in line:
         t=96.49*float(line.split()[3])
         ener.append(t)
      if "ZERO POINT ENERGY" in line:
         t=4.184*float(line.split()[3])
         ener.append(t)

   fil.close()
   return ener

#promt user 
react=int(input("Number of reactants: "))

enerr=[]
enerp=[]

#extract thermo for reactants
for i in range(react):
   fil=input("Reactant file: ")
   energ = mopace(fil)
   if fil== "C.out" or "H.out" or "F.out" or "Br.out":
      energ.append(0)
   enerr.append(energ)

#promt user
prod=int(input("Number of products: "))

#extract thermo for products
for i in range(prod):
   fil=input("Product file: ")
   energ = mopace(fil)
   if fil== "C.out" or "H.out" or "F.out" or "Br.out":
      energ.append(0)
   enerp.append(energ)

#calculate thermo reaction results productos-reactivos
results=[]
for i in range(2):
   result=0
   for j in range(prod):
      result=result+enerp[j][i]
   for j in range(react):
      result=result-enerr[j][i]
   results.append(result)
results[1]+=results[0]
print(f'ΔΕ= {results[0]}')
print(f'Δ(Ε+ZPE)= {results[1]}')
#print(results)


#de la primera lista el segundo elemento
#print(thermor[0][1])
#de la segunda lista el primer elemento
#print(thermor[1][0])
