#!/bin/bash

for i in H F Br CH CF CBr CHF CHBr CFBr CBr2 CHFBr CHBr2 CFBr2 CHFBr2
do
	TEMP=$(python3 mopac_extractor.py $i.out)
	E=$(echo $TEMP | cut -d " " -f 3)
	if [[ $TEMP =~ "ZPE" ]]; then
		ZPE=$(echo $TEMP | cut -d " " -f 6)
	else
		ZPE="0.0"
	fi
	echo "$i, $E, $ZPE" >> resultados.csv
done