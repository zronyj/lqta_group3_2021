import os
import sys
import matplotlib.pyplot as plt
import math
import matplotlib
matplotlib.rcParams['text.usetex'] = True
#it should be the output from sacamep.py
with open(sys.argv[1], "r") as f:
    data = f.readlines()

dist=[]
croot1=[]
croot2=[]
croot3=[]
troot1=[]
troot2=[]
troot3=[]

#get CASSCF energies and bond distances from scamep.py
for i in range(len(data)):
    temp=data[i].split()
    dist.append(float(temp[1]))
    croot1.append(float(temp[2]))
    croot2.append(float(temp[3]))
    croot3.append(float(temp[4]))

#it should be the output from readener.py
with open(sys.argv[2], "r") as t:
    datat = t.readlines()
#get CASPT2 energies
for i in range(len(datat)):
    temp=datat[i].split()
    troot1.append(float(temp[1]))
    troot2.append(float(temp[2]))
    troot3.append(float(temp[3]))
#energies referred to their minimum
mincroot1=min(croot1)
croot1=[2625.5*(y - mincroot1) for y in croot1]
croot2=[2625.5*(y - mincroot1) for y in croot2]
croot3=[2625.5*(y - mincroot1) for y in croot3]
#print excitation energies in the FC region
print("S1 CASSCF excitation energy")
print(croot2[0]-croot1[0])
print("S2 CASSCF excitation energy")
print(croot3[0]-croot1[0])
#same as before with CASPT2
mintroot1=min(troot1)
troot1=[2625.5*(y - mintroot1) for y in troot1]
troot2=[2625.5*(y - mintroot1) for y in troot2]
troot3=[2625.5*(y - mintroot1) for y in troot3]
print("S1 CASPT2 excitation energy")
print(troot2[0]-troot1[0])
print("S2 CASPT2 excitation energy")
print(troot3[0]-troot1[0])
#plot dissociation curves
plt.plot(dist,croot1,"rx-",label=r"S$_{0}$ CASSCF (aug-cc-pVDZ)")
plt.plot(dist,croot2,"bx-",label=r"S$_{1}$ CASSCF (aug-cc-pVDZ)")
plt.plot(dist,croot3,"kx-",label=r"S$_{2}$ CASSCF (aug-cc-pVDZ)")
plt.plot(dist,troot1,"go-",label=r"S$_{0}$ CASPT2 (aug-cc-pVDZ)")
plt.plot(dist,troot2,"co-",label=r"S$_{1}$ CASPT2 (aug-cc-pVDZ)")
plt.plot(dist,troot3,"mo-",label=r"S$_{2}$ CASPT2 (aug-cc-pVDZ)")
plt.xlabel(r"Distance (C-Br) / $\AA$")
plt.ylabel(r"Energy / $kJ \cdot mol^{-1}$")
plt.legend(loc="best")
plt.show()

