import os
import sys
import matplotlib.pyplot as plt
import math

#get root2 converged energies and iterations from the provided file(the one obtained from sacamep.py)
with open(sys.argv[1], "r") as f:
    data = f.readlines()

ener2=[]
itera=[]
tabla=[]
for i in range(len(data)):
    line=data[i].split()
    ener2.append(float(line[3]))
    itera.append(int(line[0]))
print(ener2)
print(itera)

#write converged geometries read from this file (geo.molden)
with open(sys.argv[2], "r") as ge:
    geos = ge.readlines()

it=0
for j in ener2:
    r=j+0.00000001
    for i in range(len(geos)):
        if str(j) in geos[i]:
            geom=[]
            nam="geo"+str(itera[it])+".xyz"
            for k in range(7):
                geom.append(geos[k+i-1])
            with open(nam, "w") as g:
                g.writelines(geom)
        elif str(r) in geos[i]:
            geom=[]
            nam="geo"+str(itera[it])+".xyz"
            for k in range(7):
                geom.append(geos[k+i-1])
            with open(nam, "w") as g:
                g.writelines(geom)
    it=it+1 

#write caspt2 input, path and input parameters may be changed depending on the case

for i in itera:
    inp=[]
    namein="caspt2."+str(i)+".in"
    namegeo="geo"+str(i)+".xyz"
    rasscf="/home/master99/master/M1/LQTA/project/MOLCAS/exct/MEPCASPT2/prueba2/MEPcaspt2.in."+str(i)+".RasOrb"
    inp.append(">> COPY "+rasscf+" INPORB\n")

    inp.append(" &GATEWAY\n")
    inp.append("Title= iter "+str(i)+" CHFBr2\n")
    inp.append("coord=$CurrDir/"+namegeo+"\n")
    inp.append("basis=aug-cc-pVDZ\n")
    inp.append("group=nosymm\n")

    inp.append(" &SEWARD\n")

    inp.append("&RASSCF\n")
    inp.append("   Title\n")
    inp.append("   \"MEP for root 2\"\n")
    inp.append("   Spin\n")
    inp.append("     1\n")
    inp.append("   LumOrb\n")
    inp.append("   nActEl\n")
    inp.append("     12 0 0\n")
    inp.append("   Inactive\n")
    inp.append("     37\n")
    inp.append("   Ras2\n")
    inp.append("     9\n")
    inp.append("   CiRoot\n")
    inp.append("     3 3 1\n")

    inp.append("&CASPT2\n")
    with open(namein, "w") as g:
        g.writelines(inp)
		#send calculation to the queue
    run="sbatch -A emtccm_serv -J "+namein+" -n 4 -p emtccm ./run_openmolcas_ccc "+namein
    os.system(run)     
