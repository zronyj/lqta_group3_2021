import os
import sys
#import matplotlib.pyplot as plt
import math

with open(sys.argv[1], "r") as f:
    data = f.readlines()

itera=[]
coordC=[]
coordBr=[]
dist=[]
root1=[]
root2=[]
root3=[]
tabla = ["Iter, Distancia, root1, root2, root3\n"]
tabla2=[]
iterac=0
for i in range(len(data)):
    #Calculate C-Br distance to follow the dissociation
    if "Cartesian coordinates in Angstrom:" in data[i]:
        iterac=iterac+1
        coordC=data[i+4].split()
        coordBr=data[i+8].split()
        xC=float(coordC[2])
        yC=float(coordC[3])
        zC=float(coordC[4])
        xBr=float(coordBr[2])
        yBr=float(coordBr[3])
        zBr=float(coordBr[4])
        distancia = math.sqrt(((xC-xBr)**2)+((yC-yBr)**2)+((zC-zBr)**2))
		#Get CASSCF energies
    if "Final state energy(ies):" in data[i]:
        temp1=data[i+3].split()
        temp2=data[i+4].split()
        temp3=data[i+5].split()
		#Save converged energies
    if "Geometry is converged in" in data[i]:
        tabla2.append("{0} {1} {2} {3} {4}\n".format(iterac,distancia,temp1[7],temp2[7],temp3[7]))
        dist.append(distancia)
        root1.append(float(temp1[7]))
        root2.append(float(temp2[7]))
        root3.append(float(temp3[7]))
        itera.append(iterac)

#write converged data in this file, no headings to use it in the following code       
with open("enerconv.csv", "w") as g:
    g.writelines(tabla2)

